#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

# build instructions
#
pushd source
rm -rf build
mkdir build
cd build
cmake \
	-DCMAKE_INSTALL_PREFIX=../../tmp \
	..
make -j "$(nproc)"
make install
popd

cp -rfv tmp/JediAcademy/* "6020/dist/"
